import ConsoleMessenger from './messengers/ConsoleMessenger'
import getInitialLoggingLevel from './helpers/getInitialLoggingLevel'

class Logger {
  static defaultOptions = {
    level: getInitialLoggingLevel()
  }

  constructor(options) {
    this.options = {...Logger.defaultOptions, ...options}
    this.messengers = [ConsoleMessenger]
  }

  setOptions(newOptions) {
    this.options = {...this.options, ...newOptions}
  }

  setMessengers(messengers = []) {
    this.messengers = messengers
  }

  _shouldLog(level) {
    return level <= this.options.level
  }

  log({level, messages}) {
    if (messages === null || messages === undefined) return
    if (!this._shouldLog(level)) return
    this.messengers.forEach((messenger) => {
      messenger.deliver({level, messages})
    })
  }
}

export default Logger
