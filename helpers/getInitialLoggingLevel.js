import LoggingLevel from '../LoggingLevel'

const getInitialLoggingLevel = () => {
  if (process.env.NODE_ENV === 'production') {
    return LoggingLevel.error
  }

  if (process.env.NODE_ENV === 'test') {
    return LoggingLevel.none
    // return LoggingLevel.debug
  }

  return LoggingLevel.info
}

export default getInitialLoggingLevel
