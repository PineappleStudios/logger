import Logger from './Logger'
import LoggingLevel from './LoggingLevel'

let sharedInstance

const createLogger = (options) => {
  sharedInstance = new Logger(options)
}

const getLogger = () => {
  if (sharedInstance === undefined) createLogger()
  return sharedInstance
}

const loggerForLevel = (level) => (message, ...moreMessages) => {
  getLogger().log({level, messages: [message, ...moreMessages]})
  return message
}

const logger = {
  setMessengers: (messengers) => getLogger().setMessengers(messengers),
  setOptions: (options) => getLogger().setOptions(options),
  log: loggerForLevel(LoggingLevel.info),
  error: loggerForLevel(LoggingLevel.error),
  warn: loggerForLevel(LoggingLevel.warn),
  info: loggerForLevel(LoggingLevel.info),
  debug: loggerForLevel(LoggingLevel.debug),
}

export default logger
