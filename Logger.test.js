import LoggingLevel from './LoggingLevel'

describe('logger', () => {
  let logger

  beforeEach(() => {
    logger = require('./index').default
    logger.setOptions({level: LoggingLevel.info})
    global.console = {
      warn: jest.fn(),
      info: jest.fn(),
      log: global.console.log,
      error: jest.fn(),
    }
  })

  it('should be able to handle info logging', () => {
    // Given there is a message I want to log
    const message = 'something went alright'

    // When
    logger.info(message)

    // Then
    expect(console.info).toBeCalled()
  })

  it('should be able to be chained with promises', async() => {
    // Given there is some message
    const message = 'something went alright'

    // When
    const result = await Promise.resolve(message)
      .then(logger.info)

    // Then
    expect(result).toEqual(message)
  })

  it('should be able to handle log a message', () => {
    // Given there is a message I want to log
    const message = 'some message'

    // When
    logger.log(message)

    // Then
    expect(console.info).toBeCalled()
  })

  it('should be able to handle log messages', () => {
    // Given there is a message I want to log
    const message = ['some message', 'another message']

    // When
    logger.log(...message)

    // Then
    expect(console.info).toBeCalledWith('[I] ', message[0], message[1])
  })

  it('should not print message to the console when outputsToConsole is set to false', () => {
    // Given there is a message I want to log
    const message = 'some message'
    // And the logger options are set with outputsToConsole to false
    logger.setMessengers([])

    // When
    logger.log(message)

    // Then
    expect(console.info).not.toBeCalled()
    expect(console.warn).not.toBeCalled()
    expect(console.error).not.toBeCalled()
  })

  it('should not print message when the logging level is not high enough, set with LoggingLevel', () => {
    // Given there is a message I want to log
    const message = 'some message'
    // And the logger options are set with outputsToConsole to false
    logger.setOptions({level: LoggingLevel.error})

    // When
    logger.warn(message)

    // Then
    expect(console.info).not.toBeCalled()
    expect(console.warn).not.toBeCalled()
    expect(console.error).not.toBeCalled()
  })

  it('should not print message when the logging level is not high enough, set with string', () => {
    // Given there is a message I want to log
    const message = 'some message'
    // And the logger options are set with outputsToConsole to false
    logger.setOptions({level: 'error'})

    // When
    logger.warn(message)

    // Then
    expect(console.info).not.toBeCalled()
    expect(console.warn).not.toBeCalled()
    expect(console.error).not.toBeCalled()
  })

  it('should not print message when the logging level is not high enough, set with number', () => {
    // Given there is a message I want to log
    const message = 'some message'
    // And the logger options are set with outputsToConsole to false
    logger.setOptions({level: 0})

    // When
    logger.warn(message)

    // Then
    expect(console.info).not.toBeCalled()
    expect(console.warn).not.toBeCalled()
    expect(console.error).not.toBeCalled()
  })
})
