import ConsoleMessenger from './ConsoleMessenger'

const messengers = {
  ConsoleMessenger
}

export default messengers
