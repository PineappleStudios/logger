import LoggingLevel from '../LoggingLevel'
import Messenger from './Messenger'

const ConsoleMessenger = new Messenger(({level, messages}) => {
  switch (level) {
    case LoggingLevel.error:
      return console.error('[E] ', ...messages)
    case LoggingLevel.warn:
      return console.warn('[W] ', ...messages)
    case LoggingLevel.info:
      return console.info('[I] ', ...messages)
    case LoggingLevel.debug:
      return console.info('[D] ', ...messages)
    default:
      return console.info('[I] ', ...messages)
  }
})

export default ConsoleMessenger
