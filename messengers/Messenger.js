class Messenger {
  constructor(bringFunction) {
    this.deliver = bringFunction
  }

  deliver() {
    console.log('Not implemented')
  }
}

export default Messenger
