const LoggingLevel = {
  none: -1,
  error: 0,
  warn: 1,
  info: 2,
  debug: 3
}

export default LoggingLevel
